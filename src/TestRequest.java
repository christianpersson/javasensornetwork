/**
 * Test for the Request class, Assignment 3 in Objektorienterad programmering
 * @author group18
 */
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
public class TestRequest {
	private Request req;
	private Position position;
	@Before
	public void setUp() {
		Position pos=new Position(3,4);
		this.position=pos;
		this.req = new Request(5050, 50, pos);
	}
	/**
	 * Test method for {@link Request#Request(int, int, Position)
	 */
	@Test
	public void Request() {
		Assert.assertNotNull(req);
	}
	/**
	 * Test method for {@link Request#setCurrentPos(Position)
	 */
	@Test
	public void setCurrentPos(){
		Position pos = new Position(3,5);
		req.setCurrentPos(pos);
		Assert.assertEquals(pos, req.getCurrentPos());
	}
	/**
	 * Test method for {@link Request#getCurrentPos()
	 */
	@Test
	public void getCurrentPos(){
		Assert.assertEquals(position, req.getCurrentPos());
	}
	/**
	 * Test method for {@link Request#returnLifeTime()
	 */
	@Test
	public void returnLifeTime(){
		Assert.assertEquals(50, req.returnLifeTime());
	}
	/**
	 *  Test method for {@link Request#startOver(int)
	 */
	@Test
	public void startOver(){
		Position pos = new Position(5,5);
		Node node = new Node(pos, 50, 50, 50);
		req.move(node);
		//Check that the position is changed and lifetime = original-1
		Assert.assertNotEquals(position,req.getCurrentPos());
		Assert.assertEquals(49, req.returnLifeTime());
		req.startOver(50);
		Assert.assertEquals(position, req.getCurrentPos());
		Assert.assertEquals(50, req.returnLifeTime());		
	}
	/**
	 * Test method for {@link Request#move(Node)
	 */
	@Test
	public void move(){
		Position pos = new Position(5,5);
		Node node = new Node(pos, 50, 50, 50);
		req.move(node);
		boolean samePos=false;
		//Check that the position is changed and lifetime = original-1
		Assert.assertNotEquals(position,req.getCurrentPos());
		Assert.assertEquals(49, req.returnLifeTime());
		//check that it has moved to one of the nodes neighbours
		for(Position pos1 : node.showNeighbours()){
			if(pos1 == req.getCurrentPos()){
				samePos=true;
			}
		}
		Assert.assertTrue(samePos);
	}
	
	
}
