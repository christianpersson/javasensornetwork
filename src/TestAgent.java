
/**
 * Test for the Agent class, Assignment 3 in Objektorienterad programmering
 * @author group18
 * 
 */
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
public class TestAgent {
	private Agent agent;
	Position pos;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		int id = 1000;
		pos = new Position(2,2);
		RoutingTable rt = new RoutingTable(id,pos);
		agent = new Agent(rt,45);
	}
	/**
	 * Test method for {@link Agent#Agent(RoutingTable, int)}.
	 */
	@Test
	public void testAgent() {
		assertNotNull(agent);
	}
	/**
	 * Test method for {@link Agent#move(Node, java.util.ArrayList)
	 */
	@Test
	public void testMove() {
		Throwable caught = null;
		Node node = new Node(pos,50,50,50);
		ArrayList<Position> neighbourpos=node.showNeighbours();
		//create a neihgbourlist with nodes
		ArrayList<Node> neighbourNode=new ArrayList<Node>();
		for( Position pos:neighbourpos){
			neighbourNode.add(new Node(pos,50,50,50));
		}
		try {
			node.createEvent(1001, 5);
		}catch(Throwable t){
			caught = t;
		}
		assertNull("Exception thrown when trying create event in node",caught);
		try{
			agent.move(node, neighbourNode);
		}catch(Throwable t){
			caught = t;
		}
		assertNull("Exception thrown when trying to move the agent",caught);
		assertTrue("x coordinate is wrong after move",
				   agent.getCurrentPos().getX() <= 3 && 
				   agent.getCurrentPos().getX() >= 1);
		assertTrue("y coordinate is wrong after move",
				   agent.getCurrentPos().getY() <= 3 && 
				   agent.getCurrentPos().getY() >= 1);
		assertFalse("agent has not moved after call to move()",
				   agent.getCurrentPos().getX() == 2 && 
				   agent.getCurrentPos().getY() == 2);
		assertTrue("life time has not changed to correct value",agent.returnLifeTime() == 44);
		assertTrue("corrupt merge, routing table incomplete",agent.showRoutingTable().getIDs().contains(1001));
		assertTrue("Direction incorrect after move",agent.showRoutingTable().getDirections().contains(pos));
		caught = null;
		try{
			agent.move(node,neighbourNode);
		}catch(Throwable t){
			caught = t;
		}
		assertNull("Exception thrown when trying to move the agent",caught);
		assertTrue("Distance update incorrect",agent.showRoutingTable().getDistances().contains(2));
	}
	/**
	 * Test method for {@link Agent#getCurrentPos()}.
	 */
	@Test
	public void testGetCurrentPos() {
		assertTrue("The initial position dosen't seem to be correct",
				   agent.getCurrentPos().equals(new Position(2,2)));
	}
	/**
	 * Test method for {@link Agent#setCurrentPos(Position)}.
	 */
	@Test
	public void testSetCurrentPos() {
		agent.setCurrentPos(new Position(3,3));
		assertTrue("Current position is incorrect",
				   agent.getCurrentPos().equals(new Position(3,3)));
	}
	/**
	 * Test method for {@link Agent#mergeRoutingTable(RoutingTable)}.
	 */
	@Test
	public void testMergeRoutingTable() {
		Throwable caught = null;
		RoutingTable rt = new RoutingTable(1001,new Position(1,5));
		try {
			rt.addValues(1002, 15, new Position(3,4));
		}catch(Throwable t){
			caught = t;
		}
		assertNull("Exception thrown when trying to add an id",caught);
		try{
			agent.mergeRoutingTable(rt);
		}catch(Throwable t){
			caught = t;
		}
		assertNull("Exception thrown when trying to merge tables",caught);
		assertTrue("Incorrect merge", agent.showRoutingTable().getIDs().contains(1000));
	}
	/**
	 * Test method for {@link Agent#returnLifeTime()}.
	 */
	@Test
	public void testReturnLifeTime() {
		assertTrue("The returned value is wrong",agent.returnLifeTime() == 45);
	}
}
