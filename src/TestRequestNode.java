/**
 * Test for the RequestNode class, Assignment 3 in Objektorienterad programmering
 * @author group18
 */
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
public class TestRequestNode {
	private RequestNode reqN;
	
	@Before
	public void setUp() {
		Position pos=new Position(3,4);
		this.reqN = new RequestNode(pos, 45, 50, 50, 50);
	}
	/**
	 * Test method for {@link RequestNode#RequestNode(Position, int, int, int, int)
	 */
	@Test
	public void RequestNode() {
		Assert.assertNotNull(reqN);
	}

	/**
	 * Test method for {@link RequestNode#addRequest(Request)}
	 */
	@Test
	public void addRequest(){
		Position pos = new Position(3,5);
		Request Req= new Request(5020,50,pos);
		Assert.assertTrue(reqN.showRequest().isEmpty());
		reqN.addRequest(Req);
		Assert.assertFalse(reqN.showRequest().isEmpty());
	}
	/**
	 * Test method for {@link RequestNode#showRequest()}
	 */
	@Test
	public void showRequest(){
		Position pos = new Position(3,5);
		Request Req= new Request(5020,50,pos);
		reqN.addRequest(Req);
		Assert.assertEquals(reqN.showRequest().get(0),Req);
	}
	/**
	 * Test method for {@link RequestNode#moveRequests(ArrayList)}
	 */
	@Test
	public void moveRequest(){
		//Create a request to be moved
		Position pos = new Position(3,5);
		Request Req= new Request(5020,50,pos);
		reqN.addRequest(Req);
		//check that the position is the the one inserted
		Assert.assertEquals(reqN.showRequest().get(0).getCurrentPos(),pos);
		ArrayList<Node>nodeList=new ArrayList<Node>();
		//Create a array list with possible nodes to walk to
		for(int i=0; i<9;i++){
			Position pos2=new Position(i,i);
			nodeList.add(new Node(pos2,45,50,50));
		}
		reqN.moveRequests(nodeList);
		//check is request position is changed
		Assert.assertNotEquals(reqN.showRequest().get(0).getCurrentPos(),pos);
	}
	
}