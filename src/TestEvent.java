import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.Before;
import org.junit.Test;
/**
 * Test for the Event class, Assignment 3 in Objektorienterad programmering
 * @author group18
 */
public class TestEvent {
	Event event;
	Position pos = new Position(2,2);
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		event = new Event(1000, pos, 10);
	}
	/**
	 * Test method for {@link Event#Event(int, Position, int)}.
	 */
	@Test
	public void testEvent() {
		assertNotNull("constructor returns null",event);
	}
	/**
	 * Test method for {@link Event#getID()}.
	 */
	@Test
	public void testGetID() {
		assertTrue("getID() returns wrong value",event.getID() == 1000);
	}
	/**
	 * Test method for {@link Event#getPos()}.
	 */
	@Test
	public void testGetPos() {
		assertTrue("position returned is incorrect",event.getPos() == pos);
	}
	/**
	 * Test method for {@link Event#getTime()}.
	 */
	@Test
	public void testGetTime() {
		assertTrue("time returned is incorrect",event.getTime() == 10);
	}
	/**
	 * Test method for {@link Event#printEvent()}.
	 */
	@Test
	public void testPrintEvent() {
		event.printEvent();
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));
		event.printEvent();
		assertEquals("Event ID: 1000  position: 2,2  time: 10\n", 
	    		outContent.toString());
	}
}
