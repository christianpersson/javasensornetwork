/**
 * Event class for Assignment 3 in Objektorienterad programmering
 * @author group18
 */

public class Event {
	// Event ID-tag, position and time
	private int ID;
	private Position position;
	private int time;
	
	/**
	 * Constructs a new event with given ID, position and time.
	 * @param id		ID-tag of event
	 * @param pos		Position of event
	 * @param t			Time of event
	 */
	public Event(int id, Position pos, int t){
		this.ID = id;
		this.position = pos;
		this.time = t;
	}
	
	/**
	 * Returns the event ID.
	 * @return ID			the event ID.
	 */
	public int getID(){
		return ID;
	}
	
	/**
	 * Returns the event position.
	 * @return position		the event position.
	 */
	public Position getPos(){
		return position;
	}
	
	/**
	 * Returns the event time.
	 * @return time 		the event time.
	 */
	public int getTime(){
		return time;
	}
	
	/**
	 * Prints out the event attributes.
	 */
	public void printEvent(){
		System.out.print("Event ID: " + ID);
		System.out.print("  position: " + position.getX() + "," + position.getY());
		System.out.println("  time: " + time);
	}
}
