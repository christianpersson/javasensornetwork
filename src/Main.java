


/**
 * 
 *
 */
public class Main {

	public static void main(String[] args){
		
		int requests = 0;
		int steps = 10000;
		Environment envir = new Environment( 50, 50, 0.0001, 45, 50);
		for(int time=0;time<steps;time++){
			envir.stepTime(time);
			if(time%500 == 0)
				System.out.println(time);
			if(time%400 == 0){
				envir.createRequest();
				requests++;
			}
		}
		System.out.println("# of requests: "+requests*4);
	}
}
