import java.util.ArrayList;

/**
 * Position class for Assignment 3 in Objektorienterad programmering
 * @author group18
 */

public class Position {
	// x and y coordinates
	private int x;
	private int y;
	
	/**
	 * Constructs a new position with x and y as values.
	 * @param x		the x coordinate.
	 * @param y		the y coordinate. 
	 */
	Position(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Returns the x value.
	 * @return x 	The x value.
	 */
	public int getX(){
		return x;
	}
	
	/**
	 * Returns the y value.
	 * @return y 	The y value.
	 */
	public int getY(){
		return y;
	}
	
	/**
	 * Collects the closets neighbours in a list. Number of collected 
	 * neighbours is between 3 - 8. Neighbours which x or y coordinate 
	 * is neqative is not collected. 
	 * @return neighbours 		An ArrayList with neighbours.
	 */
	public ArrayList<Position> getNeighbours(int rows, int columns){
		ArrayList<Position> neighbours = new ArrayList<Position>();
		// Get all neighbours.
		Position arr[]=new Position[8];
		arr[0] = new Position(this.x-1,this.y+1);
		arr[1] = new Position(this.x-1,this.y);
		arr[2] = new Position(this.x-1,this.y-1);
		arr[3] = new Position(this.x,this.y-1);
		arr[4] = new Position(this.x+1,this.y+1);
		arr[5] = new Position(this.x+1,this.y);
		arr[6] = new Position(this.x+1,this.y-1);
		arr[7] = new Position(this.x,this.y+1);
		//add only the ones in network to list
		for(int i=0;i<8;i++){
			if(arr[i].getX()>=0 && arr[i].getX()<rows){
				if(arr[i].getY()>=0 && arr[i].getY()<columns){
					neighbours.add(arr[i]);
				}
			}

		}
		return neighbours;
	}
	
	/**
	 * Generates a positions hash code.
	 * @return result		The hash code.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}
	
	/**
	 * Checks if two positions are identical.
	 * @return True if the positions are equal. 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}	
}

