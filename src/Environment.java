import java.util.ArrayList;
import java.util.Random;

/** 
 * Agent class for Assignment 3 in Objectoriented programmering
 * @author group18
 */
public class Environment {
	private int rows;
	private int cols;
	private int requestLifeLength;
	private int agentLifeLength;
	private double eventProbability;
	private double agentProbability;
	private ArrayList<Integer> IDList;
	private ArrayList<Position> requestNodeList;
	private Node[][] network;
	private ArrayList<Agent> agentList;
	
	/**
	 * Constructs a new environment.
	 * @param rows					number of rows in nodematrix
	 * @param cols					number of columns in nodematrix
	 * @param eventProbability		chance for node to detect event
	 * @param requestLifeLength		amount of steps a request can exist without finding way to searched event
	 * @param agentLiftLength		amount of steps an agent can exist
	 */
	public Environment(int rows, int cols, double eventProbability,
			int requestLifeLength, int agentLifeLength){
		this.rows = rows;
		this.cols = cols;
		this.eventProbability = eventProbability;
		this.agentProbability = 0.500;
		this.requestLifeLength = requestLifeLength;
		this.agentLifeLength = agentLifeLength;
		
		IDList = new ArrayList<Integer>();
		requestNodeList = new ArrayList<Position>();
		agentList = new ArrayList<Agent>();
		this.network = new Node[this.rows][this.cols];
		int i,j;
		int numReqNodes = 4;
		Random ran = new Random();
		ArrayList<Position> reqNodes = new ArrayList<Position>(); 
		// generate random positions for requestnodes
		for(i=0;i<numReqNodes;i++){
			Position pos;
			do{
				pos = new Position(ran.nextInt(this.rows),ran.nextInt(this.cols));
			}while(reqNodes.contains(pos));
			reqNodes.add(pos);
		}
		for(j=0;j<this.cols;j++){
			for(i=0;i<this.rows;i++){
				Position pos = new Position(i,j);
				//constructing requestnodes
				if(reqNodes.contains(pos)){
					RequestNode reqNode = new RequestNode(pos, this.agentLifeLength, this.requestLifeLength, this.rows, this.cols);
					this.network[i][j] = reqNode;
					this.requestNodeList.add(pos);
				}
				//constructing nodes
				else
					this.network[i][j] = new Node(pos,this.agentLifeLength, this.rows, this.cols);
			}
		}
	}
	
	/**
	 * Constructs an environment with parameters from assignment 3.
	 */
	public Environment(){
		this( 50, 50, 0.0001, 45, 50);
	}
	/**
	 * Method used in stepTime by nodes when event is detected
	 * @return	returns a unique integer as an ID
	 */
	public int generateID(){
		Random ran = new Random();
		int ID = ran.nextInt(10000001);
		while(this.IDList.contains(ID)){
			ID = ran.nextInt(10000001);
		}
		this.IDList.add(ID);
		return ID;
	}
	
	/**
	 * Method which should be used when creating requests
	 * can only create requests if at least one event has occurred
	 */
	public void createRequest(){
		if(!this.IDList.isEmpty()){
			Random ran = new Random();
			for(int i=0;i<this.requestNodeList.size();i++){
				int id = this.IDList.get(ran.nextInt(this.IDList.size()));
				Position pos = this.requestNodeList.get(i);
				RequestNode reqNode =(RequestNode) this.network[pos.getX()][pos.getY()];
				reqNode.addRequest(new Request(id, this.requestLifeLength, pos));
			}
		}
	}
	
	/**
	 * Use this method when checking if a node exists in the network
	 * @param pos	Position of the node which will be checked if exists in network
	 * @return		return boolean if the node exists
	 */
	public boolean nodeExists(Position pos){
		if(pos == null)
			return false;
		if(pos.getX() < 0 || pos.getY() < 0
				|| pos.getX() >= this.rows 
				|| pos.getY() >= this.cols)
			return false;
		else
			return true;
	}

	/**
	 * Method which takes one step in time
	 */
	public void stepTime(int time){

		if(!this.agentList.isEmpty()){
			for(int i=0;i<this.agentList.size();i++){
				Agent bond = this.agentList.get(i);
				Position pos = bond.getCurrentPos();

				if(bond.returnLifeTime() > 0){

					try {
						// merge
						this.network[pos.getX()][pos.getY()].mergeRoutingTable(bond.showRoutingTable());
						
						// neighbor-nodes
						ArrayList<Node> neighborList = new ArrayList<Node>();
						for(Position neighbourPos : this.network[pos.getX()][pos.getY()].showNeighbours()){
							neighborList.add(this.network[neighbourPos.getX()][neighbourPos.getY()]);
						}
						
						// If agent first in queue
						if(this.network[pos.getX()][pos.getY()].agentQueue.indexOf(bond) == 0){
							Node newPos = bond.move(this.network[pos.getX()][pos.getY()], neighborList);
							// If the agent moved successfully
							if(bond.hasMoved()){
								newPos.enQueueAgent(bond);
								this.network[pos.getX()][pos.getY()].disEnQueueAgent(bond);
							}
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}	
				}else{
					this.network[pos.getX()][pos.getY()].disEnQueueAgent(bond);
					this.agentList.remove(bond);
				}
			}
		}
		for(int i=0;i<this.requestNodeList.size();i++){
			Position pos = this.requestNodeList.get(i);
			RequestNode reqNode = (RequestNode)this.network[pos.getX()][pos.getY()];
			ArrayList<Request> reqList = reqNode.showRequest();
			ArrayList<Node> nodeList = new ArrayList<Node>();
			for(int j=0;j<reqList.size();j++){
				Request req = reqList.get(j);
				Position posReq = req.getCurrentPos();
				nodeList.add(this.network[posReq.getX()][posReq.getY()]);
			}
			reqNode.moveRequests(nodeList);
		}
		Random ran = new Random();
		for(int j=0;j<this.cols;j++){
			for(int i=0;i<this.rows;i++){
				// set all nodes to available
				this.network[i][j].makeAvailable();
				if(ran.nextDouble() <= this.eventProbability){
					int ID = this.generateID();
					try {
						this.network[i][j].createEvent(ID, time);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(ran.nextDouble() <= this.agentProbability){
						agentList.add(this.network[i][j].createAgent(ID));
					}
				}
			}
		}
	}
	
	/**
	 * helpmethod ONLY used for testing
	 * @return	IDList
	 */
	protected ArrayList<Integer> showIDList(){
		return this.IDList;
	}
	
	/**
	 * helpmethod ONLY used for testing
	 * @return	RequestNodeList
	 */
	protected ArrayList<Position> showRequestNodes(){
		return this.requestNodeList;
	}
	
	/**
	 * helpmethod ONLY used for testing
	 * @return	Nodematrix
	 */
	protected Node[][] showNodes(){
		return this.network;
	}
}
