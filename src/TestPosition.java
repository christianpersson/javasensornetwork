import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
/**
 * Test for the Position class, Assignment 3 in Objektorienterad programmering
 * @author group18
 */
public class TestPosition {
	private Position pos;
	private int x = 3;
	private int y = 2;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		pos =  new Position(x,y);
	}

	/**
	 * Test method for {@link Position#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		Position pos2 = new Position(x,y);
		assertTrue("HashCode of two identical positions are not the same",pos.hashCode() == pos2.hashCode());
		pos = new Position(x+1,y-1);
		assertTrue("HashCode of two different positions are the same",pos.hashCode() != pos2.hashCode());
	}

	/**
	 * Test method for {@link Position#Position(int, int)}.
	 */
	@Test
	public void testPosition() {
		assertNotNull("Position is null after construction",pos);
	}
	/**
	 * Test method for {@link Position#getX()}.
	 */
	@Test
	public void testGetX() {
		assertEquals("Could not get x value.",x,pos.getX());
	}
	/**
	 * Test method for {@link Position#getY()}.
	 */
	@Test
	public void testGetY() {
		assertEquals("Could not get y value.",y,pos.getY());
	}
	/**
	 * Test method for {@link Position#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		Position pos2 = new Position(x,y);
		Position pos3 = new Position(x+1,y+1);
		assertTrue("Two equal positions returns false",pos.equals(pos2) && pos2.equals(pos));
		assertFalse("Two different positions returns true",pos.equals(pos3) && pos3.equals(pos));
	}
	/**
	 * Test method for {@link Position#getNeighbours()}.
	 */
	@Test
	public void testGetNeighbours() {
		pos = new Position(2,2);
		ArrayList<Position> neighbours = new ArrayList<Position>();
		neighbours = pos.getNeighbours(50,50);
		// Check so all eight neighbours are found
		assertTrue("Not correct number of neighbours in list",neighbours.size() == 8);
		// Check so all positions are correct
		for(int x=1;x<=3;x++){
			for(int y=1;y<=3;y++){
				if(x!=2 && y!=2){
					assertTrue("Does not contain all expected neighbours.",neighbours.contains(new Position(x,y)));
				}
			}
		}
	}
	/**
	 * Test method for {@link Position#getNeighbours()}.
	 */
	@Test
	public void testGetCornerNeighbours() {
		pos = new Position(0,0);
		ArrayList<Position> neighbours = new ArrayList<Position>();
		neighbours = pos.getNeighbours(50,50);
		// Check so all eight neighbours are found
		assertTrue("Not correct number of neighbours in list",neighbours.size() == 3);
		// Check so all positions are correct
		for(int x=0;x<=1;x++){
			for(int y=0;y<=1;y++){
				if(x!=0 && y!=0){
					assertTrue("Does not contain all expected neighbours.",neighbours.contains(new Position(x,y)));
				}
			}
		}
	}
}
