import java.util.ArrayList;

/** 
 * Agent class for Assignment 3 in Objektorienterad programmering.
 * @author group18
 */

public class Agent {
	private Position position;
	private RoutingTable routingTable;
	private int lifeTime;
	private ArrayList<Position> visitedNodes = new ArrayList<Position>();
	private boolean isEnqueued;
	private Node nextPosition;
	private boolean moved;
	
	/**
	 * Constructs a new Agent.
	 * Note: Collects the current position from the routing table.
	 * @param 	routingTable		The nodes RoutingTable.
	 * @param 	lifeTime			The agents lifetime.
	 */
	public Agent(RoutingTable routingTable, int lifeTime){
		this.isEnqueued = false;
		this.moved = false;
		this.lifeTime = lifeTime;
		this.routingTable = routingTable;
		// Get index of line in routingTable with distance zero
		int index = routingTable.getDistances().lastIndexOf(0);
		// Get the current position from the routingTable
		position = routingTable.getDirections().get(index);
		// add current position to visited nodes
		visitedNodes.add(position);
	}
	
	/**
	 * Moves the agent to one random neighbour. It tries to move to an 
	 * unvisited position. 
	 * @param node				The current node.
	 * @param neighborList		The neighboring nodes.
	 * @return					The node it moved to.
	 * @throws Exception
	 */
	public Node move(Node node, ArrayList<Node> neighborList) throws Exception{
		
		// Update routing table
		this.routingTable.merge(node.showRoutingTable());
		// If not in queue
		if (!this.isEnqueued){
			// Look for any unvisited neighbors
			ArrayList<Position> neighbours =  node.showNeighbours();
			// Indexes of unvisited neighbours instead
			ArrayList<Integer> unvisitedNeighbours = new ArrayList<Integer>();
			for(Position neighbourPos : neighbours){
				if(!visitedNodes.contains(neighbourPos)){
					unvisitedNeighbours.add(neighbours.indexOf(neighbourPos)); 
				}	
			}
			Position newPos;
			// Check if there where any unvisited neighbours
			if((unvisitedNeighbours.size())>0){
				// Get position from random unvisited neighbour
				int index = unvisitedNeighbours.get((int)(Math.random()*unvisitedNeighbours.size()));
				newPos = neighbours.get(index);
				// Check if neighbour available, if not enqueue and save "direction" in nextPosition.
				if(neighborList.get(index).isAvailable()){
					visitedNodes.add(newPos);
					// Update routingTable with new distances and directions.
					for(int ind=0;ind < this.routingTable.getIDs().size();ind++){
						this.routingTable.changeValues(this.routingTable.getIDs().get(ind), 
								this.routingTable.getDistances().get(ind)+1, this.getCurrentPos());
					}
					this.setCurrentPos(newPos);
					this.moved = true;
					neighborList.get(index).agentEnter();
					node.agentLeaving();
					lifeTime--;
				}
				else{
					// Store the "direction" if the neighbour is busy.
					this.isEnqueued = true;
					this.moved = false;
					this.nextPosition = neighborList.get(index);
				}
				return neighborList.get(index);
				
			}else{
				// Get position from random visited neighbour.
				int index = (int)(Math.random()*neighbours.size());
				newPos = neighbours.get(index);
				if(neighborList.get(index).isAvailable()){
					// Update routingTable with new distances and directions.
					for(int ind=0;ind < this.routingTable.getIDs().size();ind++){
						this.routingTable.changeValues(this.routingTable.getIDs().get(ind), 
								this.routingTable.getDistances().get(ind)+1, this.getCurrentPos());
					}
					this.setCurrentPos(newPos);
					this.moved = true;
					neighborList.get(index).agentEnter();
					node.agentLeaving();
					lifeTime--;
				}
				else{
					// Store the "direction" if the neighbour is busy.
					this.isEnqueued = true;
					this.moved = false;
					this.nextPosition = neighborList.get(index);
				}
				return neighborList.get(index);
			}
		}
		else{
			// Try to go to the neighbour who was busy last time.
			int index = neighborList.indexOf(this.nextPosition);
			
			if(neighborList.get(index).isAvailable()){
				// Update routingTable with new distances and directions.
				for(int ind=0;ind < this.routingTable.getIDs().size();ind++){
					this.routingTable.changeValues(this.routingTable.getIDs().get(ind), 
							this.routingTable.getDistances().get(ind)+1, this.getCurrentPos());
				}
				this.setCurrentPos(neighborList.get(index).showPosition());
				this.moved = true;
				neighborList.get(index).agentEnter();
				node.agentLeaving();
				lifeTime--;
				this.isEnqueued = false;
			}
			else{
				// Store the "direction" if the neighbour is busy.
				this.isEnqueued = true;
				this.moved = false;
				this.nextPosition = neighborList.get(index);
			}
			return neighborList.get(index);
		}
		
	}
	
	/**
	 * Changes the current position.
	 * @param position		The new position
	 */
	public void setCurrentPos(Position position){
		this.position = position;
	}
	
	/**
	 * Returns the current position.
	 * @return			The current position
	 */
	public Position getCurrentPos(){
		return position;
	}
	
	/**
	 * Merge two routing tables.
	 * @param routingTable		The RoutingTable to merge with.
	 * @throws Exception 
	 * @see RoutingTable#merge(RoutingTable)
	 */
	public void mergeRoutingTable(RoutingTable routingTable) throws Exception{
		this.routingTable.merge(routingTable);
	}
	
	/**
	 * Returns the remaining life time.
	 * @return lifeTime			The remaining life time.
	 */
	public int returnLifeTime(){
		return lifeTime;
	}
	
	/**
	 * Returns the agents RoutingTable.
	 * @return RoutingTable 	The RoutingTable of this agent.
	 */
	public RoutingTable showRoutingTable(){
		return this.routingTable;
	}
	
	/**
	 * Checks if the agent have moved.
	 * @return			True if the agent have moved, false otherwise.
	 */
	public boolean hasMoved(){
		return this.moved;
	}
}

