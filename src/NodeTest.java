/**
 * Test for the Node class, Assignment 3 in Objektorienterad programmering
 * @author group18
 */
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
public class NodeTest {
	Node node;
	Position pos;
	@Before
	public void setUp() {
		this.pos=new Position(3,4);
		int agentLifeTime=50;
		int rows=50;
		int cols=50;
		this.node=new Node(pos,agentLifeTime,rows, cols );
	}
	/**
	 * Test method for {@link Node#Node(Position, int, int, int)
	 */
	@Test
	public void Node(){
		Assert.assertNotNull(node);
	}
	/**
	 * Test method for {@link Node#createEvent(int, int)
	 */
	@Test
	public void createEvent(){
		try {
			node.createEvent(5040, 50);
		} catch (Exception e) {
			Assert.fail("Exception");
		}
		System.out.println(node.EventList.size());
		Event event=new Event(5040, pos,50);
		Assert.assertEquals(node.routingTable.getDirections().get(0),event.getPos());
		Assert.assertEquals(node.EventList.get(0).getID(),event.getID());
		
	}
	/**
	 * Test method for {@link Node#mergeRoutingTable(RoutingTable)
	 */
	@Test
	public void mergeRoutingTable(){
		RoutingTable rt=new RoutingTable(5040, pos);
		try {
			node.mergeRoutingTable(rt);
		} catch (Exception e) {
			Assert.fail("Exception");
		}
		Assert.assertEquals(node.routingTable.getIDs(),rt.getIDs());
		Assert.assertEquals(node.routingTable.getDirections(),rt.getDirections());
		Assert.assertEquals(node.routingTable.getDistances(),rt.getDistances());
	}
	/**
	 * Test method for {@link Node#showRoutingTable()
	 */
	@Test
	public void showRoutingTable(){
		RoutingTable rt=new RoutingTable(5040, pos);
		try {
			node.routingTable.addValues(5040, 0, pos);
		} catch (Exception e) {
			Assert.fail("Exception");
		}
		Assert.assertEquals(rt.getIDs(),node.showRoutingTable().getIDs());
		Assert.assertEquals(rt.getDirections(),node.showRoutingTable().getDirections());
		Assert.assertEquals(rt.getDistances(),node.showRoutingTable().getDistances());
	}
	/**
	 * Test method for {@link Node#createAgent(int)
	 */
	@Test
	public void createAgent(){
		Agent agent=node.createAgent(5040);
		Assert.assertNotNull(agent);
	}
	/**
	 * Test method for {@link Node#enQueueAgent(Agent)
	 */
	@Test
	public void enQueueAgent(){
		Agent agent=node.createAgent(5040);
		node.enQueueAgent(agent);
		Assert.assertTrue(node.agentQueue.contains(agent));		
	}
	/**
	 * Test method for {@link Node#disEnQueueAgent(Agent)
	 */
	@Test
	public void disEnQueueAgent(){
		Agent agent=node.createAgent(5040);
		Assert.assertTrue(node.agentQueue.contains(agent));		
		node.disEnQueueAgent(agent);
		Assert.assertFalse(node.agentQueue.contains(agent));
	}
	/**
	 * Test method for {@link Node#isAvailable()
	 */
	@Test
	public void isAvialable(){
		Assert.assertTrue(node.isAvailable());
	}
	/**
	 * Test method for {@link Node#agentEnter()
	 */
	@Test
	public void agentEnter(){
		node.agentEnter();
		Assert.assertFalse(node.isAvailable());
	}
	/**
	 * Test method for {@link Node#agentLeaving()
	 */
	@Test
	public void agentLeaving(){
		node.agentEnter();
		node.agentLeaving();
		Assert.assertFalse(node.isAvailable());
	}
	/**
	 * Test method for {@link Node#showNeighbours()
	 */
	@Test
	public void showNeighbours(){
		ArrayList<Position> neighbours=node.showNeighbours();
		Assert.assertEquals(8, neighbours.size());
	}
	/**
	 * Test method for {@link Node#makeAvailable()
	 */
	@Test
	public void makeAvailable(){
		node.agentEnter();
		node.makeAvailable();
		Assert.assertTrue(node.isAvailable());
	}
	/**
	 * Test method for {@link Node#showPosition()
	 */
	@Test
	public void showPosition(){
		Assert.assertEquals(pos, node.showPosition());
	}

}
