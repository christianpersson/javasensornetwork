import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
/**
 * Test for the RoutingTable class, Assignment 3 in Objektorienterad programmering
 * @author group18
 */
public class TestRoutingTable {
	RoutingTable rt;
	Position pos;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		pos = new Position(2,2);
		rt = new RoutingTable(1000, pos);
	}
	/**
	 * Test method for {@link RoutingTable#RoutingTable(int, Position)}.
	 */
	@Test
	public void testRoutingTableIntPosition() {
		assertNotNull("Routing table is null after call to constructor",rt);
	}
	/**
	 * Test method for {@link RoutingTable#RoutingTable()}.
	 */
	@Test
	public void testRoutingTable() {
		RoutingTable rt2 = new RoutingTable();
		assertNotNull("Routing table is null after call to constructor",rt2);
	}
	/**
	 * Test method for {@link RoutingTable#addValues(int, int, Position)}.
	 */
	@Test
	public void testAddValues() {
		Throwable caught = null;
		Position newpos = new Position(3,3);
		try{
			rt.addValues(1001, 10, newpos);
		}catch(Throwable t){
			caught = t;
		}
		assertNull("Exception thrown when trying to add an id",caught);
		
		assertTrue("Added ID does not exist",        rt.getIDs().contains(1000) && 
												     rt.getIDs().contains(1001));
		assertTrue("Added distance does not exist",  rt.getDistances().contains(0) && 
												     rt.getDistances().contains(10));
		assertTrue("Added direction does not exist", rt.getDirections().contains(pos) && 
				 								     rt.getDirections().contains(newpos));
		caught = null;
		try{
			rt.addValues(1001, 10, newpos);
		}catch(Throwable t){
			caught = t;
		}
		assertNotNull("No exception thrown when trying to add an already existing id",caught);
		assertSame("Wrong exception thrown",Exception.class,caught.getClass());
	}
	/**
	 * Test method for {@link RoutingTable#changeValues(int, int, Position)}.
	 */
	@Test
	public void testChangeValues() {
		Throwable caught = null;
		Position newpos = new Position(3,3);
		try{
			rt.changeValues(1000, 10, newpos);
		}catch(Throwable t){
			caught = t;
		}
		assertNull("Exception thrown when trying to change an id",caught);
		assertTrue("Changed ID does not exist",         rt.getIDs().contains(1000));
		assertTrue("Changed distance does not exist",  !rt.getDistances().contains(0) && 
												        rt.getDistances().contains(10));
		assertTrue("Changed direction does not exist", !rt.getDirections().contains(pos) && 
				 								        rt.getDirections().contains(newpos));
		caught = null;
		try{
			rt.changeValues(1001, 10, newpos);
		}catch(Throwable t){
			caught = t;
		}
		assertNotNull("No exception thrown when trying to change an non-existing id",caught);
		assertSame("Wrong exception thrown",Exception.class,caught.getClass());
	}
	/**
	 * Test method for {@link RoutingTable#getIDs()}.
	 */
	@Test
	public void testGetIDs() {
		assertTrue("incorrect id list returned",rt.getIDs().contains(1000));
	}
	/**
	 * Test method for {@link RoutingTable#getDistances()}.
	 */
	@Test
	public void testGetDistances() {
		assertTrue("incorrect distance list returned",rt.getDistances().contains(0));
	}
	/**
	 * Test method for {@link RoutingTable#getDirections()}.
	 */
	@Test
	public void testGetDirections() {
		assertTrue("incorrect direction list returned",rt.getDirections().contains(pos));
	}
	/**
	 * Test method for {@link RoutingTable#merge(RoutingTable)}.
	 */
	@Test
	public void testMerge() {
		Throwable caught = null;
		try{
			rt.changeValues(1000, 10, pos);
		}catch(Throwable t){
			caught = t;
		}
		assertNull("Exception thrown when trying to change an id",caught);
		Position newpos = new Position(3,3);
		RoutingTable rt2 = new RoutingTable(1001,newpos);
		try{
			rt.merge(rt2);
		}catch(Throwable t){
			caught = t;
		}
		assertNull("Exception thrown when trying to merge an id",caught);

		assertTrue("Merged ID does not exist",        rt.getIDs().contains(1000) && 
			     									  rt.getIDs().contains(1001));
		assertTrue("Merged distance does not exist",  rt.getDistances().contains(0) && 
			     									  rt.getDistances().contains(10));
		assertTrue("Merged direction does not exist", rt.getDirections().contains(pos) && 
			     									  rt.getDirections().contains(newpos));
	}

}
