import java.util.ArrayList;

/**
 * RequestNode class for Assignment 3 in Objektorienterad programmering.
 * @author group18
 */

public class RequestNode extends Node{
	private ArrayList<Request> requestList;
	private ArrayList<Integer> timeSinceLastRequest;
	private int requestLifetime;
	
	/**
	 * Constructs a new RequestNode.
	 * @param Position 			The position of this Node.
	 * @param agentLifeTime		The life time of the agents created.
	 * @param reqLifeTime		The life time of the requests created.
	 * @param rows				Number of rows in the network.
	 * @param columns			Number of columns in the network.
	 */
	public RequestNode(Position position, int agentLifeTime, int reqLifeTime, int rows, int columns){
		super(position, agentLifeTime, rows, columns);
		this.requestList=new ArrayList<Request>();
		this.timeSinceLastRequest=new ArrayList<Integer>();		
		this.requestLifetime=reqLifeTime;
	}
	
	/**
	 * Moves the requests created in this node.
	 * @param nodes		The nodes at which the requests currently are positioned.
	 */
	public void moveRequests(ArrayList<Node> nodes){
		for(int i=0;i<this.requestList.size();i++){
			Request req = this.requestList.get(i);
			int index = this.requestList.indexOf(req);
			// Check if the request has returned
			if(req.isBack){
				req.event.printEvent();
				this.requestList.remove(index);
				this.timeSinceLastRequest.remove(index);
				nodes.remove(index);
				this.requestList.trimToSize();
				this.timeSinceLastRequest.trimToSize();
				nodes.trimToSize();
				i--;
			}
			// Check if request should be sent Again.
			else if(this.timeSinceLastRequest.get(index) >= 8 * this.requestLifetime){	//is returnLifeTime condition needed?
				if(req.hasBeenReSent){
					this.requestList.remove(index);
					this.timeSinceLastRequest.remove(index);
					nodes.remove(index);
					this.requestList.trimToSize();
					this.timeSinceLastRequest.trimToSize();
					nodes.trimToSize();
				}
				else{
					this.timeSinceLastRequest.set(index,0);
					req.setCurrentPos(this.position);
					req.startOver(this.requestLifetime);
					req.move(nodes.get(index));
					this.timeSinceLastRequest.set(index, this.timeSinceLastRequest.get(index) + 1);
				}
			}
			else{
				if(req.returnLifeTime() > 0){
					req.move(nodes.get(index));
				}
				this.timeSinceLastRequest.set(index, this.timeSinceLastRequest.get(index) + 1);
			}

		}
	}
	
	/**
	 * Returns the requests created by this node.
	 * @return requestList		A list of this nodes requests.
	 */
	public ArrayList<Request> showRequest(){
		return this.requestList;
	}
	
	/**
	 * Adds a request to this node.
	 * @param request		The request to add.
	 */
	public void addRequest(Request request){
		this.requestList.add(request);
		this.timeSinceLastRequest.add(0);
	}
}