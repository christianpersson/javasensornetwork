import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

/** 
 * Request class for Assignment 3 in Objektorienterad programmering
 * @author group18
 */

public class Request {
	private int id;
	private int lifeTime;
	private Stack<Position> path = new Stack<Position>();
	protected Event event;
	private boolean eventFound;
	protected boolean hasBeenReSent;
	public boolean isBack;
	
	
	/**
	 * Constructs a new Request with ID to look for, a lifetime and starting position.
	 * @param id			Event ID to search for.
	 * @param lifeTime		Lifetime of request.
	 * @param position		The starting position of the request.
	 */
	public Request(int id, int lifeTime, Position position){
		this.id = id;
		this.lifeTime = lifeTime;
		this.path.push(position);
		this.eventFound = false;
		this.hasBeenReSent=false;
		this.isBack=false;
	}
	
	/**
	 * Moves the request one step. If the event is not found, it moves to a random neighbor.
	 * If a road to an event is found the request moves in the direction suggested by the current node.
	 * If an event is found, the request follows the path it took to get to the current node back to the starting node.
	 * @param node		The node that currently are visited by the request.
	 */
	public void move(Node node){
		if(eventFound){ 
			// Event is found (going back to the RequestNode)
			if(!path.isEmpty()){
				path.pop();
				if(path.isEmpty()){
					this.isBack=true;
					path.push(new Position(1,1));
				}
			}
		}else if(node.routingTable.getIDs().contains(this.id)){
			// Event-track is found (going to where the Event occurred).
			int index= node.routingTable.getIDs().indexOf(this.id);
			if(node.routingTable.getDistances().get(index)==0){
				this.eventFound=true;
				int eventindex = 0;
				for(Event E : node.EventList){
					if(E.getID() == this.id){
						eventindex = node.EventList.indexOf(E);
					}
				}
				this.event = node.EventList.get(eventindex);
				if(!path.isEmpty()){
					path.pop();
					if(path.isEmpty()){
						this.isBack=true;
						path.push(new Position(1,1));
					}
				}
			}
			else {
				this.setCurrentPos(node.routingTable.getDirections().get(index));
			}	
		}
		else {
			// Before event is found (or event-track) (random).
			ArrayList<Position> unvisitedNeighbours=new ArrayList<Position>();
			// Check if any neighbours have been visited.
			for(int index=0;index<node.Neighbours.size();index++){
				if(path.search(node.Neighbours.get(index)) < 0){
					unvisitedNeighbours.add(node.Neighbours.get(index)); 
				}	
			}
			// Check if there are any unvisited neighbours.
			if((unvisitedNeighbours.size())>0){
				// Get position from random unvisited neighbour.
				Random rand = new Random();
				Position newPos = unvisitedNeighbours.get(rand.nextInt(unvisitedNeighbours.size()));
				// Put the new position in path-stack.
				path.push(newPos);
				this.lifeTime=this.lifeTime -1;
			}else{
				// Get position from random visited neighbour.
				Position newPos = node.Neighbours.get((int)(Math.random()*node.Neighbours.size()));
				// Put the new position in path-stack.
				path.push(newPos);
				this.lifeTime=this.lifeTime-1;
			}
		}
			
	}
	
	/**
	 * Changes the current position.
	 * @param position		The new position
	 */
	public void setCurrentPos(Position position){
		path.push(position);
	}
	
	/**
	 * Returns the current position.
	 * @return position		The current position
	 */
	public Position getCurrentPos(){
		return path.peek();
	}
	
	/**
	 * Returns the remaining life time.
	 * @return lifetime			The remaining life time.
	 */
	public int returnLifeTime(){
		return lifeTime;
	}
	
	/**
	 * Should be used when a request is sent a second time.
	 * -Clears the stack containing the path. 
	 * -Sets the lifeTime to original value.
	 * -Sets the boolean hasBeenReSent to true.
	 * @param reqlifeTime	The lifetime of the request.
	 */
	public void startOver(int reqlifeTime){
		while(this.path.size() > 1){
			this.path.pop();
		}
		this.lifeTime=reqlifeTime;
		this.hasBeenReSent=true;
	}
}


