import java.util.ArrayList;

/**
 * RoutingTable class for Assignment 3 in Objektorienterad programmering.
 * @author group18
 */

public class RoutingTable {
	// Lists of event ID:s, distances and directions.
	private ArrayList<Integer> ID;
	private ArrayList<Integer> distance;
	private ArrayList<Position> direction;
	
	/**
	 * Constructs a new RoutingTable with an event ID.
	 * @param id 			Event ID.
	 * @param position		Position of event or road to event.
	 */
	public RoutingTable(int id, Position position){
		this.ID = new ArrayList<Integer>();
		this.distance = new ArrayList<Integer>();
		this.direction = new ArrayList<Position>();
		this.ID.add(id);
		this.distance.add(0);
		this.direction.add(position);
	}
	
	/**
	 * Constructs a new empty RoutingTable.
	 */
	public RoutingTable(){
		this.ID = new ArrayList<Integer>();
		this.distance = new ArrayList<Integer>();
		this.direction = new ArrayList<Position>();
	}
	
	/**
	 * Adds information about an event in the routing table.
	 * @param id			Event ID.
	 * @param distance		Distance to event.
	 * @param direction		Direction to event.
	 * @throws Exception 
	 */
	public void addValues(int id, int distance, Position direction) throws Exception{
		if(!this.ID.contains(id)){
			this.ID.add(id);
			int index = this.ID.indexOf(id);
			this.distance.add(index, distance);
			this.direction.add(index, direction);
		}else{
			throw new Exception("id does already exist in routing table");
		}
	}
	
	/**
	 * Changes information about an event in the routingtable.
	 * @param id			Event ID.
	 * @param distance		Distance to event.
	 * @param direction		Direction to event.
	 * @throws Exception 
	 */
	public void changeValues(int id, int distance, Position direction) throws Exception{
		int index = this.ID.indexOf(id);
		if(index > -1){
			this.distance.set(index, distance);
			this.direction.set(index, direction);
		}else{
			throw new Exception("id does not exist in routing table");
		}
	}
	
	/**
	 * Returns a list of event IDs.
	 * @return ID 			List of event IDs.
	 */
	public ArrayList<Integer> getIDs(){
		return this.ID;
	}
	
	/**
	 * Returns a list of distances.
	 * @return distance 	List of distances.
	 */
	public ArrayList<Integer> getDistances(){
		return this.distance;
	}
	
	/**
	 * Returns a list of directions.
	 * @return direction	List of directions.
	 */
	public ArrayList<Position> getDirections(){
		return this.direction;
	}
	
	/**
	 * Merge the current routing table with another. After a merge both 
	 * tables will be identical.
	 * @param routingTable	The routing table to merge with.
	 * @throws Exception 
	 */
	public void merge(RoutingTable routingTable) throws Exception{
		int index;
		// For every ID in the "other" routing table check if it already exists.
		for(int id : routingTable.getIDs()){
			index = routingTable.getIDs().indexOf(id);
			// If it doesn't exist -> add it to the table.
			if(!this.ID.contains(id)){
				this.addValues(id, routingTable.getDistances().get(index),
						            routingTable.getDirections().get(index));
			}
			// If it exists but the new distance is shorter, change it.
			else{ 
				int ownIndex = this.ID.indexOf(id);
				if(this.distance.get(ownIndex) > routingTable.getDistances().get(index)){
					this.changeValues(id, routingTable.getDistances().get(index),
							routingTable.getDirections().get(index));
				}
			}
		}		
	}
}

