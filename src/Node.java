import java.util.ArrayList;
/**
 * Node class for Assignment 3 in Objektorienterad programmering.
 * @author group18
 */

public class Node {
	protected Position position;
	protected ArrayList<Position> Neighbours;
	protected RoutingTable routingTable;
	protected ArrayList<Agent> agentQueue;
	protected int agentLifeTime;
	protected ArrayList<Event> EventList;
	protected boolean Avialable;
	
	/**
	 * Constructs a new Node
	 * @param Pos	 			The position of this Node.
	 * @param agentLifeT		The life time the agents created.
	 * @param rows				Number of rows in network.
	 * @param columns			Number of columns in network.
	 */
	public Node(Position Pos, int agentLifeT, int rows, int columns) {
		this.position=Pos;
		this.agentLifeTime=agentLifeT;
		this.Neighbours= Pos.getNeighbours(rows,columns);
		this.Avialable=true;
		this.EventList=new ArrayList<Event>();
		this.agentQueue=new ArrayList<Agent>();
		this.routingTable= new RoutingTable();
		
	}
	
	/**
	 * Creates a new Event and adds it to the EventList of this node.
	 * @param ID 		The event ID.
	 * @param Time		The time of the event, in timesteps.
	 * @throws Exception 
	 */
	public void createEvent(int ID,int Time) throws Exception{
		this.EventList.add(new Event(ID, this.position,Time));
		this.routingTable.addValues(ID, 0, this.position);
	}
	
	/**
	 * Merge two routing tables.
	 * @param routingTable			The RoutingTable to merge with.
	 * @throws Exception 
	 * @see RoutingTable#merge(RoutingTable)
	 */
	public void mergeRoutingTable(RoutingTable routingTable) throws Exception{
		this.routingTable.merge(routingTable);
	}
	/**
	 * Returns the nodes RoutingTable.
	 * @return RoutingTable 	The RoutingTable of this node.
	 */
	public RoutingTable showRoutingTable(){
		return this.routingTable;
	}
	/**
	 * After calling on this method an Agent will be created.
	 * @return Agent 		The created agent.
	 */
	public Agent createAgent(int ID){
		RoutingTable rt=new RoutingTable(ID, this.position);
		Agent agent = new Agent(rt, this.agentLifeTime);
		this.agentQueue.add(agent);
		return agent;
	}
	/**
	 * Adds the agent to the agentQueue. Should be used when this 
	 * node is unavailable and a agent tries to enter.
	 * @param Agent		Agent to put in agentQueue.
	 */
	public void enQueueAgent(Agent agent){
		this.agentQueue.add(agent);
	}
	/**
	 * Removes the Agent from the agentQueue. Should be used when killing an agent.
	 * @param Agent		Agent to remove.
	 */
	public void disEnQueueAgent(Agent agent){
		this.agentQueue.remove(agent);
		this.agentQueue.trimToSize();
	}
	/**
	 * Returns true if the node is not already occupied by an Agent. 
	 * @return boolean	  True if Available, false if not.
	 */
	public boolean isAvailable(){
		return this.Avialable;
	}
	/**
	 * Use this method when an agent enter the node.
	 * Sets the node to not available.
	 */
	public void agentEnter(){
		this.Avialable=false;
	}
	/**
	 * Use this method when agent leaves the node. 
	 * Sets the node to not available.
	 */
	public void agentLeaving(){
		this.Avialable=false;
	}
	/**
	 *  Returns an ArrayList with positions to the neighbours of the node.
	 *  @return ArrayList<Position> 	The positions of the nodes neighbours.
	 */
	public ArrayList<Position> showNeighbours(){
		return this.Neighbours;
	}
	/**
	 * Returns the position of this node.
	 * @return Position		The nodes position.
	 */
	public Position showPosition(){
		return this.position;
	}
	/**
	 * Makes the node Available for agents. 
	 */
	public void makeAvailable(){
		this.Avialable = true;
	}
	
}
