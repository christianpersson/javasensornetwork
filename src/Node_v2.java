/**
 * Node class for Assignment 3 in Objektorienterad programmering
 * @author group18
 * Last edited by oi10nan, 20140520
 */

//TODO: UML protected... createaGent probability???

// TODO: 
// -CreateAgent must have ID as an parameter!
// -Is agentProbability needed in this class or should enviroment handle it instead?
// -isAvaialable function, how should it work? attribut which an agent changes 
//  when entering the node and when leaving or is there a simpler way?
// -Need to create an empty routingTable!? 
// -copyRoutingTable? merge instead? will not do this method until discussed. 
// -should the eventCreate also add to the routing table? right now it does.
import java.util.ArrayList;
public class Node {
	protected Position position;
	protected ArrayList<Position> Neighbours;
	protected RoutingTable routingTable;
	protected ArrayList<Agent> agentQueue;
	protected int agentLifeTime;
	protected ArrayList<Event> EventList;
	protected boolean Avialable;
	/**
	 * Constructs a new Node
	 * @param Position 			The position of this Node.
	 * @param agentLifeTime		The life time the agents created.
	 * @param agentProbability	The Probability of an agent being created.
	 */
	public Node(Position Pos, int agentLifeT, int rows, int columns) {
		this.position=Pos;
		this.agentLifeTime=agentLifeT;
		this.Neighbours= Pos.getNeighbours(rows,columns);
		this.Avialable=true;
		this.EventList=new ArrayList<Event>();
		this.agentQueue=new ArrayList<Agent>();
		this.routingTable= new RoutingTable();
		
	}
	
	/**
	 * Creates a new Event and adds it to the EventList of this node.
	 * @param ID 		The event ID.
	 * @param Time		The time of the event, in timesteps.
	 * @throws Exception 
	 */
	public void createEvent(int ID,int Time) throws Exception{
		this.EventList.add(new Event(ID, this.position,Time));
		this.routingTable.addValues(ID, 0, this.position);
	}
	
	/**
	 * Merge two routing tables.
	 * @param routingTable
	 * @throws Exception 
	 * @see RoutingTable#merge(RoutingTable)
	 */
	public void mergeRoutingTable(RoutingTable routingTable) throws Exception{
		this.routingTable.merge(routingTable);
	}
	/**
	 * Returns the nodes RoutingTable
	 * 
	 * @return RoutingTable 	The RoutingTable of this node.
	 */
	public RoutingTable showRoutingTable(){
		return this.routingTable;
	}
	/**
	 * After calling on this method an Agent will be created.
	 *  
	 * @return Agent 		
	 */
	public Agent createAgent(int ID){
		// skapa med prob.
		RoutingTable rt=new RoutingTable(ID, this.position);
		/*NEW*/
		Agent agent = new Agent(rt, this.agentLifeTime);
		// add agent to queue
		this.agentQueue.add(agent);
		return agent;
	}
	/**
	 * Add the agent to the agentQueue. Should be used when this 
	 * node is unavailable and a agent tries to enter.   
	 * 
	 * @param Agent		
	 */
	public void enQueueAgent(Agent agent){
		this.agentQueue.add(agent);
	}
	/*NEW*/
	/**
	 * Add the agent to the agentQueue. Should be used when this 
	 * node is unavailable and a agent tries to enter.   
	 * 
	 * @param Agent		
	 */
	public void disEnQueueAgent(Agent agent){
		this.agentQueue.remove(agent);
		this.agentQueue.trimToSize();
	}
	/**
	 * Returns true if the node is not already occupied by an Agent. 
	 * @return boolean	  True if Available, false if not.
	 */
	public boolean isAvailable(){
		return this.Avialable;
	}
	
	/**
	 * Use this method when an agent enter the node.
	 * 
	 */
	public void agentEnter(){
		this.Avialable=false;
	}
	/**
	 * Use this method when agent leaves the node. 
	 */
	/*NEW*/
	public void agentLeaving(){
		this.Avialable=false;
	}
	/**
	 *  Returns an ArrayList with positions to the neighbours of the node.
	 *  @return ArrayList<Position> 
	 */
	public ArrayList<Position> showNeighbours(){
		return this.Neighbours;
	}
	
	/*NEW*/
	public Position showPosition(){
		return this.position;
	}
	
	/*NEW*/
	public void makeAvailable(){
		this.Avialable = true;
	}
	
}
