import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
/**
 * Test for the Environment class, Assignment 3 in Objektorienterad programmering
 * @author group18
 */

public class EnvironmentTest {
	
	private Environment envir;

	@Before
	public void setUp(){
		envir = new Environment(4, 4, 1.00, 45, 50);
	}
	
	/**
	 * test for constructor.
	 */
	@Test
	public void testEnvironment() {
		assertNotNull("Environment not created!",envir);
	}
	
	/**
	 * test for generateID
	 */
	@Test
	public void testGenerateID(){
		int ID = envir.generateID();
		assertTrue("Wrong ID in list!",envir.showIDList().contains(ID));
		int ID2 = envir.generateID();
		assertFalse("Same ID was generated!\n",ID == ID2);
	}
	
	/**
	 * test for nodeExists
	 */
	@Test
	public void testNodeExists(){
		assertTrue("existing node was not found!\n",envir.nodeExists(new Position(1,1)));
		assertFalse("not existing node was found!\n",envir.nodeExists(new Position(-1,-1)));
	}
	
	/**
	 * test for createRequest
	 */
	@Test
	public void testCreateRequest(){
		int numReq  = 0;
		envir.generateID();
		envir.createRequest();
		ArrayList<Position> reqNPos = envir.showRequestNodes();
		ArrayList<RequestNode> reqNodes = new ArrayList<RequestNode>();
		for(int i=0;i<reqNPos.size();i++){
			reqNodes.add((RequestNode)envir.showNodes()[reqNPos.get(i).getX()][reqNPos.get(i).getY()]);
		}
		for(int i=0;i<reqNodes.size();i++){
			for(int j=0;j<reqNodes.get(i).showRequest().size();j++){
				reqNodes.get(i).showRequest().get(j);
				numReq++;
			}
		}
		assertEquals("The number of requests created was wrong!\n",reqNPos.size(),numReq);
	}
}
